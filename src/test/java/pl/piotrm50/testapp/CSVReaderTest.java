package pl.piotrm50.testapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.InputMismatchException;

import org.junit.Ignore;
import org.junit.Test;

public class CSVReaderTest {

    @Test
    public final void correctPathShouldReturnObjectTest() {
        CSVReader reader = new CSVReader("titanic");
        assertNotNull(reader.readCSVFile("Titanic.csv"));
    }

    @Ignore
    @Test
    public final void wrongPathShouldReturnNullTest() {
        CSVReader reader = new CSVReader("titanic");
        assertNotNull(reader.readCSVFile("wrongpath.csv"));
    }

    @Test
    public final void nullPathShouldReturnNullTest() {
        CSVReader reader = new CSVReader("titanic");
        assertNull(reader.readCSVFile(null));
    }

    @Test
    public final void hasCorrectNumberOfColumnsWhenElementIsBroken() {
        CSVReader reader = new CSVReader("titanic");
        reader.headers = new String[6];
        String input = "\"22\",\"2nd\",\"Female,\"Child\",\"Yes\",13";
        assertEquals(reader.splitRow(input).length, 6);
    }

    @Test
    public final void hasCorrectNumberOfColumnsWhenRowIsCorrect() {
        CSVReader reader = new CSVReader("titanic");
        reader.headers = new String[6];
        String input = "\"22\",\"2nd\",\"Female\",\"Child\",\"Yes\",13";
        assertEquals(reader.splitRow(input).length, 6);
    }

    @Test(expected = InputMismatchException.class)
    public final void throwsInputMismatchExceptionWhenMissingDelimiter() {
        CSVReader reader = new CSVReader("titanic");
        reader.headers = new String[6];
        String input = "\"22\",\"2nd\",\"Female\"\"Child\",\"Yes\",13";
        reader.splitRow(input);
    }
}
