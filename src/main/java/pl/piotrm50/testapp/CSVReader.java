package pl.piotrm50.testapp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

public class CSVReader {
    String[] headers;
    MongoDatabase mongoDB;
    MongoClient mongoClient;
    SolrClient solr;

    public static void main(String[] args) {
        CSVReader reader = new CSVReader("titanic");
        // reader.listMongo("titanic");
        // reader.updateMongo(new Document("Sex", "Kobieta"), new Document("$set", new
        // Document("Sex", "Female")), "titanic");
        // reader.listMongo("titanic");
        // reader.mongoClient.close();
        // reader.listSolr("titanic");
        reader.addReadDataToDBs(reader.readCSVFile("Titanic.csv"));
        reader.listSolr("Crew");
    }

    public CSVReader(String dbName) {
        // mongoClient = new MongoClient();
        // mongoDB = mongoClient.getDatabase(dbName);
        solr = new HttpSolrClient("http://localhost:8983/solr/titanicTest");
    }

    public ArrayList<String[]> readCSVFile(String path) {
        if (path == null) {
            return null;
        }
        ArrayList<String[]> result = new ArrayList<>();
        Scanner scan = null;
        try {
            scan = new Scanner(new File(path));
            headers = splitRow(scan.nextLine());
            while (scan.hasNextLine()) {
                String[] row = splitRow(scan.nextLine());
                result.add(row);
            }

        } catch (FileNotFoundException e) {
            return null;
        } finally {
            if (scan != null) {
                scan.close();
            }
        }
        return result;
    }

    public String[] splitRow(String input) {
        String[] row = input.split(",");
        if (headers != null && row.length != headers.length) {
            throw new InputMismatchException("Too few columns in a row.");
        }

        for (int i = 0; i < row.length; i++) {
            row[i] = StringUtils.remove(row[i], '"');
        }
        return row;
    }

    public void insertToMongo(String[] data, String collection) {
        Document entry = new Document("_id", Integer.parseInt(data[0]));
        for (int i = 1; i < data.length; i++) {
            entry.append(headers[i], data[i]);
        }
        mongoDB.getCollection(collection).insertOne(entry);
    }

    public void insertToSolr(String[] data) {
        SolrInputDocument input = new SolrInputDocument();
        input.addField("id", data[0]);
        for (int i = 1; i < data.length; i++) {
            input.addField(headers[i], data[i]);
        }
        try {
            solr.add(input);
            solr.commit();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

    }

    public void addReadDataToDBs(ArrayList<String[]> data) {
        for (String[] row : data) {
            // insertToMongo(row, "titanic");
            insertToSolr(row);
        }
    }

    public void listMongo(String collection) {
        FindIterable<Document> iterable = mongoDB.getCollection(collection).find();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document);
                System.out.println(document.get("Class"));
            }
        });
    }

    public void listSolr(String query) {
        SolrQuery params = new SolrQuery();
        params.set("q", query);
        try {
            SolrDocumentList response = solr.query(params).getResults();
            for (int i = 0; i < response.size(); i++) {
                System.out.println(response.get(i));
            }
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

    }

    public void updateMongo(Document match, Document update, String collection) {
        mongoDB.getCollection(collection).updateMany(match, update);
    }
}
